APL-Library
===========

APL is a functional programming language originally proposed in the
1950s and implemented by IBM in the 1960s.  GNU APL is a copyleft
interpreter of the language.  These workspaces offer functions not
specified by IS13551, the international standard specification for APL.

APL uses a suite of operators and functions represented by special
characters and many APL vendors use proprietary file formats.  Files
in the library are utf-8 encoding where APL's special characters are
stored with their Unicode value. 

To display these files one must have an appropriate font installed.
The Free Software Foundation offers one at http://ftp.gnu.org/gnu/unifont/

As the standard computer keyboard does not contain many of these
special APL characters you must reconfigure yours and you have many
options. File README-3-keyboard distributed with GNU APL offers many
ideas. I used Emacs and gnu-apl-mode.

Documentation
-------------
Open file apl-library.info by entering `info path/to/apl-library.info`
at the shell prompt.


license
-------
Copyright (C) 2016, 2017, 2018 2019, 2020, 2021 and 2022 Bill Daly.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

